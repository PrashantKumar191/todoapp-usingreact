import React, { Component } from "react";
class Header extends Component {
  render() {
    const {onThemeChange,themeIcon}=this.props;
    return (
      <header className="header">
        TODO
        <img
          onClick={onThemeChange}
          src={
            themeIcon
              ? "/images/icon-sun.svg"
              : "/images/icon-moon.svg"
          }
          alt="theme toggle icon"
          id="theme-image"
        />
      </header>
    );
  }
}

export default Header;
