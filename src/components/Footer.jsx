import React, { Component } from "react";

const filters = [
  {
    label: "All",
  },
  {
    label: "Active",
  },
  {
    label: "Completed",
  },
];
export default class Footer extends Component {
  render() {
    const { count, boxColor, onFilterChange, onClearComplete } = this.props;
    return (
      <footer id="functions" className={boxColor ? "box-light" : "box-dark"}>
        <span id="counter">
          <span id="count-value">{count} items left</span>
        </span>
        <ul id="status">
          {filters.map((filter) => {
            return (
              <li
                key={filter.label}
                id={filter.label}
                onClick={() => onFilterChange(filter)}
              >
                {filter.label}
              </li>
            );
          })}
        </ul>
        <span className="clear-all" onClick={onClearComplete}>
          Clear Completed
        </span>
      </footer>
    );
  }
}
