import React, { Component } from "react";

export default class InputBar extends Component {
  state = {
    task: "",
  };

  handleChange = (e) => {
    this.setState({ task: e.target.value });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.onInput(this.state.task);
    this.setState({ task: "" });
  };
  render() {
    return (
      <div
        id="input-bar"
        className={this.props.boxColor ? "box-light" : "box-dark"}
      >
        <form onSubmit={this.handleSubmit} autoComplete="off">
          <input type="submit" value="" />
          <input
            type="text"
            name=""
            id="new-item"
            placeholder="Create a new todo..."
            value={this.state.task}
            onChange={this.handleChange}
          />
        </form>
      </div>
    );
  }
}
