import React, { Component } from "react";

export default class ListContainer extends Component {
  render() {
    const { tasks, onRemove, boxColor } = this.props;
    return (
      <div id="list-container"  className={boxColor? "box-light":"box-dark"}>
        <ul id="list-items">
          {tasks.map((task) => {
            return (
              <li key={task.id} className="list-item">
                <input
                  type="checkbox"
                  id={task.id}
                  checked={task.complete}
                  className="custom-checkbox"
                  onClick={() => this.props.onToggle(task.id)}
                  onChange={(e) => {}}
                />
                <span className="tick-mark">
                  <img src="/images/icon-check.svg" alt="" />
                </span>
                <label htmlFor={task.id}>{task.title}</label>
                <img
                  src="/images/icon-cross.svg"
                  className="delete"
                  id={task.id}
                  alt="delete"
                  onClick={() => onRemove(task.id)}
                />
              </li>
            );
          })}
        </ul>
      </div>
    );
  }
}
