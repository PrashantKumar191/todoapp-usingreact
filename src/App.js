import "./App.css";
import React, { Component } from "react";
import { v4 as uuid } from "uuid";
// importing components
import Header from "./components/Header";
import InputBar from "./components/Input-Bar";
import ListContainer from "./components/ListContainer";
import Footer from "./components/Footer";

class App extends Component {
  state = {
    tasks: [],
    toRender: "All",
    theme: true,
  };
  // Change theme
  handleThemeChange = () => {
    this.setState({
      theme: !this.state.theme,
    });
  };

  // For Adding tasks in todo-list
  handleInputTodo = (taskInput) => {
    if (taskInput !== "") {
      let task = {
        title: taskInput,
        id: uuid(),
        complete: false,
      };
      let tasks = [...this.state.tasks, task];
      this.setState({
        tasks: tasks,
      });
    }
  };

  // For check and uncheck tasks
  handleToggle = (toggleId) => {
    this.setState({
      tasks: this.state.tasks.map((task) => {
        if (task.id === toggleId) {
          return {
            ...task,
            complete: !task.complete,
          };
        }
        return task;
      }),
    });
  };

  // For removing task using cross button
  handleRemove = (taskId) => {
    let tasks = this.state.tasks.filter((task) => task.id !== taskId);
    this.setState({ tasks: tasks });
  };

  // For removing completed tasks
  handleClearComplete = () => {
    let tasks = this.state.tasks.filter((task) => task.complete === false);
    this.setState({ tasks: tasks });
  };

  // To change the filter All/Active/Completed
  handleFilterChange = (filter) => {
    this.setState({ toRender: filter.label });
  };

  // For displaying the selected filter
  handleDisplay = (filterTodisplay) => {
    if (filterTodisplay === "All") {
      return this.state.tasks;
    } else if (filterTodisplay === "Completed") {
      return this.state.tasks.filter((task) => task.complete === true);
    } else if (filterTodisplay === "Active") {
      return this.state.tasks.filter((task) => task.complete === false);
    } else {
      return [];
    }
  };
  render() {
    const taskToRender = this.handleDisplay(this.state.toRender);
    const tasksRemainig = this.state.tasks.filter(
      (task) => task.complete === false
    ).length;
    return (
      <React.Fragment>
        <div
          className={this.state.theme ? "lightBackground" : "darkBackground"}
        >
          <div id="wrapper">
            <div id="container">
              <Header
                onThemeChange={this.handleThemeChange}
                themeIcon={this.state.theme}
              />
              <InputBar
                onInput={this.handleInputTodo}
                boxColor={this.state.theme}
              />
              <ListContainer
                boxColor={this.state.theme}
                tasks={taskToRender}
                onToggle={this.handleToggle}
                onRemove={this.handleRemove}
              />
              <Footer
                boxColor={this.state.theme}
                count={tasksRemainig}
                onFilterChange={this.handleFilterChange}
                onClearComplete={this.handleClearComplete}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default App;
